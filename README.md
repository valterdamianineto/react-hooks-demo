# React Hooks

<p align="center" fontSize="60px">
  Several examples using react hooks
</p>


## Project

The project demonstrates the structure, configuration and usage of several hooks including custom hooks.


##  Technologies

-  [JavaScript](https://www.javascript.com/)
-  [React](https://pt-br.reactjs.org/)
-  [Typescript](https://www.typescriptlang.org/)


## 📥 Installation and execution

Clone this repository.

```bash
# To run this project it is necessary to have Node installed, as well as Yarn or NPM

# Installing dependencies
$ npm i

# Running application
$ npm start

# The system default execution port is:
$ http://localhost:3000/
