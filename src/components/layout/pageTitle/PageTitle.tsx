import './PageTitle.css';

interface Props {
    error?: boolean;
    title?: string;
    subtitle?: string;
}

const PageTitle = (props: Props) => (
    <div className={`PageTitle ${props.error ? "error" : ""}`}>
        <h1>{props.title}</h1>
        <h2>{props.subtitle}</h2>
    </div>
)

export default PageTitle