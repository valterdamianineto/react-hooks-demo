import './SectionTitle.css'

interface Props {
    title: string
}

const SectionTitle = (props: Props) => (
    <div className="SectionTitle">
        <h3>{props.title}</h3>
    </div>
)

export default SectionTitle