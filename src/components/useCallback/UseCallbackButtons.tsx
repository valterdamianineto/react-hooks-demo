import React from "react";

interface Props {
    inc: (n: number) => void;
    setCount: (n: number) => void;
}

const useCallbackButtons = (props: Props) => {
    return (
            <>
            <button onClick={() => props.setCount(0)} className="btn">Reset</button>
            <button onClick={() => props.inc(6)} className="btn">+ 6</button>
            <button onClick={() => props.inc(12)} className="btn">+ 12</button>
            <button onClick={() => props.inc(18)} className="btn">+ 18</button>
            </>
        )
}

export default React.memo(useCallbackButtons);