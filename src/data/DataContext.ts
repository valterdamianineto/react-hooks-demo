import React, { SetStateAction } from 'react'

export type DataProps = {
  number: number,
  text: string,
  setNewState?: React.Dispatch<SetStateAction<DataProps>>
}

export const data: DataProps = {
    number: 123,
    text: 'Context Api Example',
}

const DataContext = React.createContext(data)

export default DataContext
