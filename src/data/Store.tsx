import React, { SetStateAction, useState } from 'react';

export type DataProps = {
    number: number,
    text: string,
    setState?: React.Dispatch<SetStateAction<DataProps>>
    setNumber?: React.Dispatch<string | number> | undefined
    setText?: React.Dispatch<string | number> | undefined
  }

interface Props {
    children: React.ReactNode;
}

const initialState: DataProps = {
    number: 1234,
    text: 'Context API + Hooks'
};

export const AppContext = React.createContext(initialState);

const Store = (props: Props) => {
    const [state, setState] = useState<DataProps>(initialState);

    function updateState(key: number | string, newValue: number | string) {
        setState({
            ...state, 
            [key]: newValue
        });
    }
    return (
        <AppContext.Provider value={{
            number: state.number,
            text: state.text,
            setNumber: n => updateState('number', n),
            setText: t => updateState('text', t),
        }}>
            {props.children}
        </AppContext.Provider>
    )    
}

export default Store