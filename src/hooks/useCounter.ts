import { useState } from 'react';

type CustomHook = {
    count: number;
    inc: () => void;
    dec: () => void;
}

function useCounter(initialvalue = 100): CustomHook {
    const [count, setCount] = useState<number>(initialvalue)

    function inc() {
        setCount(count + 1)
    }

    function dec() {
        setCount(count - 1)
    }

    return {count, inc, dec}
}

export default useCounter