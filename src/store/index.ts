import { State } from '../types'
import { numberAdd2 } from './actions/number'
import { login } from './actions/user'
import reducer from './reducers'

const initialState: State = {
    number: 1,
    cart: [],
    products: [],
    user: null
}

export {
    reducer,
    initialState,
    numberAdd2,
    login
}

