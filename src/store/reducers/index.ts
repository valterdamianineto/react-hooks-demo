import { State } from '../../types'

export default function reducer(state: State, action: any) {
    switch (action.type) {
        case 'login_example':
            return {
                ...state,
                user: {name: 'Daniel Ricciardo', email: 'envkt@example.com'}
            }
        case 'login_example_2':
            return {
                ...state,
                user: {name: action.name}
            }
        case 'login_example_3':
            return {
                ...state,
                user: {name: action.payload.name, email: action.payload.email}
            }
        case 'number_add_1':
            return {
                ...state,
                number: state.number + 1
            }
        case 'number_add_2':
            return {
                ...state,
                number: state.number + 2
            }
        default:
            return state;
    }
}
