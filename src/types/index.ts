export type User = {
    name: string;
    email?: string;
}

export type State = {
    number: number;
    cart: Array<any>;
    products: Array<any>;
    user: User | null;
}