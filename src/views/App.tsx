import { BrowserRouter as Router } from 'react-router-dom'
import './App.css'

import { useState } from 'react'
import Content from '../components/layout/content/Content'
import Menu from '../components/layout/menu/Menu'
import DataContext, { data, DataProps } from '../data/DataContext'
import Store from '../data/Store'
 
const App = () => {
    const [state, setNewState] = useState<DataProps>(data)

    return (
        <div className="App">
            <Store>
                <DataContext.Provider value={{...state, setNewState}}>
                    <Router>
                        <Menu />
                        <Content />
                    </Router>
                </DataContext.Provider>
            </Store>
        </div>
    )
}

export default App
 