import PageTitle from '../../components/layout/pageTitle/PageTitle'

const Home = () => (
    <div className="Home">
        <PageTitle
            title="React Hooks"
            subtitle="Demonstration of how react hooks modules work" />
    </div>
)

export default Home