import { useCallback, useState } from 'react'
import PageTitle from '../../components/layout/pageTitle/PageTitle'
import SectionTitle from '../../components/layout/sectionTitle/SectionTitle'
import UseCallbackButtons from '../../components/useCallback/UseCallbackButtons'

const UseCallback = () => {
    const [count, setCount] = useState(0)

    // function inc(delta: number) {
    //     setCount(count + delta)
    // }

    const inc = useCallback(function(delta: number) {
        setCount(currentCount => currentCount + delta)
    }, [setCount])
    
    return (
        <div className="UseCallback">
            <PageTitle
                title="Hook UseCallback"
                subtitle="Retorna uma função memoizada!"
            />
            <SectionTitle title="Use Callback #1"/>
            <div className="center">
                <span className="text">{count}</span>
                <div>
                    <UseCallbackButtons inc={inc} setCount={setCount}/>
                </div>
            </div>
        </div>
    )
}

export default UseCallback
