import { useContext } from 'react';
import PageTitle from '../../components/layout/pageTitle/PageTitle';
import SectionTitle from '../../components/layout/sectionTitle/SectionTitle';
import DataContext from '../../data/DataContext';
import { AppContext } from '../../data/Store';

const UseContext = () => {
    const context = useContext(DataContext)
    
    function addNumber(delta: any): void {
        context.setNewState?.({
            ...context,
            number: context.number + delta
        })
    }

    const { text, number, setNumber } = useContext(AppContext)

    
    return (
        <div className="UseContext">
            <PageTitle
                title="Hook UseContext"
                subtitle="Aceita um objeto de contexto e retorna o valor atual do contexto!"
            />

            <SectionTitle title="Use Context #1"/>
            <div className="center">
                <span className="text">{context.text}</span>
                <span className="text">{context.number}</span>
                <div>
                    <button className="btn" onClick={() => addNumber(-1)}>-1</button>
                    <button className="btn" onClick={() => addNumber(+1)}>+1</button>
                </div>
            </div>
            <SectionTitle title="Use Context #2"/>
            <div className='center'>
                <span className="text">{ text }</span>
                <span className="text">{number}</span>
                <div>
                    <button className="btn" onClick={() => setNumber?.(number - 100)} >- 100</button>
                    <button className="btn" onClick={() => setNumber?.(number - 10)} >- 10</button>
                    <button className="btn" onClick={() => setNumber?.(number - 1)} >- 1</button>
                    <button className="btn" onClick={() => setNumber?.(number + 1)}>+ 1</button>
                    <button className="btn" onClick={() => setNumber?.(number + 10)}>+ 10</button>
                    <button className="btn" onClick={() => setNumber?.(number + 100)}>+ 100</button>
                </div>
            </div>

        </div>
    )
}

export default UseContext
