import PageTitle from '../../components/layout/pageTitle/PageTitle';
import SectionTitle from '../../components/layout/sectionTitle/SectionTitle';
import useCounter from '../../hooks/useCounter';
import { useFetch } from '../../hooks/useFetch';

interface StateItem {
    nome: string;
    sigla: string;
}

const UseRef = () => {
    const {count, inc, dec} = useCounter()
    const url = 'http://files.cod3r.com.br/curso-react/estados.json'
    const response = useFetch(url)

    function showStates(statesList: Array<StateItem>): any {
        statesList.map(state => 
            <li key={state.nome}>{state.nome} - {state.sigla}</li>
        )
    }

    return (
        <div className="UseCustom">
            <PageTitle
                title="Seu Hook"
                subtitle="Vamos aprender como criar o nosso próprio Hook!"
            />
            <SectionTitle title="Custom Hook #1"/>
            <div className="center">
                <span className="text">{count}</span>
                <div>
                    <button className="btn" onClick={() => inc()}>Inc</button>
                    <button className="btn" onClick={() => dec()}>Dec</button>
                </div>
            </div>
            <SectionTitle title="Custom Hook"/>
            <div className="center">
                <ul>
                   {
                    response.data ? showStates(response.data)
                    : null    
                    } 
                </ul>
            </div>
        </div>
    )
}

export default UseRef
