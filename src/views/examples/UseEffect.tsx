import { useEffect, useState } from 'react'
import PageTitle from '../../components/layout/pageTitle/PageTitle'
import SectionTitle from '../../components/layout/sectionTitle/SectionTitle'

function calFactorial(n: number): number {
    if(n < 0) return -1
    if(n === 0) return 1
    return calFactorial(n - 1) * n
}


const UseEffect = () => {
    const [number, setNumber] = useState<number>(1)
    const [factorial, setFactorial] = useState(1)
    const [status, setStatus] = useState(0)
    const [currentNumber, setCurrentNumber] = useState(0)
        

    useEffect(function () {
        setFactorial(calFactorial(number))
    }, [number])

    useEffect(function() {
        if(factorial > 100000) {
            setNumber(0)
        }
    }, [factorial])

    useEffect(function() {
        if(currentNumber % 2 === 0) {
            setStatus(1)
        }else {
            setStatus(0)
        }
    }, [currentNumber])


    return (
        <div className="UseEffect">
            <PageTitle
                title="Hook UseEffect"
                subtitle="Permite executar efeitos colaterais em componentes funcionais!"
            />

            <SectionTitle title="Use Effect"/>
            <div className="center">
                <span className="text">Factorial</span>
                <span className="text red">{factorial === -1 ? 'Does not Exist': factorial}</span>
                <input type="number" className="input" value={number} onChange={e => setNumber(+e.target.value)}/>
            </div>

            <SectionTitle title="Use Effect"/>
            <div className="center">
                <span className="text">Even or Odd</span>
                <span className="text red">{status === 1 ? 'Even': 'Odd'}</span>
                <input type="number" className="input" value={currentNumber} onChange={e => setCurrentNumber(+e.target.value)}/>
            </div>
        </div>
    )
}

export default UseEffect
