import { useMemo, useState } from 'react'
import PageTitle from '../../components/layout/pageTitle/PageTitle'
import SectionTitle from '../../components/layout/sectionTitle/SectionTitle'

function sum(a: number, b: number): number {
    const future = Date.now() + 2000
    while(Date.now() < future) {}
    return a + b
}

const UseMemo = () => {
    const [n1, setN1] = useState(0)
    const [n2, setN2] = useState(0)
    const [n3, setN3] = useState(0)
    
        // const [result, setResult] = useState(0)
        // useEffect(function() {
        //     setResult(sum(n1, n2))
        // }, [n1, n2])

        const result = useMemo(() => sum(n1, n2), [n1, n2])

    return (
        <div className="UseMemo">
            <PageTitle
                title="Hook UseMemo"
                subtitle="Retorna um valor memoizado!"
            />
            <SectionTitle title="Use Memo #1"/>
            <div className="center">
                <span className="text">{result}</span>
                <input type="number" className="input" value={n1} onChange={e => setN1(parseInt(e.target.value))}/>
                <input type="number" className="input" value={n2} onChange={e => setN2(parseInt(e.target.value))}/>
                <input type="number" className="input" value={n3} onChange={e => setN3(parseInt(e.target.value))}/>
            </div>
        </div>
    )
}

export default UseMemo
