import { useReducer, useState } from 'react';
import PageTitle from '../../components/layout/pageTitle/PageTitle';
import SectionTitle from '../../components/layout/sectionTitle/SectionTitle';
import { initialState, reducer } from '../../store';
import { login, numberAdd2 } from '../../store/actions';
import { State } from '../../types';

function numberController(state2: State, action: any) {
    switch (action.type) {
        case 'add_custom':
            return {
                ...state2,
                number: state2.number + action.payload.number
            }
        case 'multiply_by_7':
            return {
                ...state2,
                number: state2.number * 7
            }
        case 'divide_by_25':
            return {
                ...state2,
                number: state2.number / 25
            }
        case 'parse_number':
            return {
                ...state2,
                number: Math.floor(state2.number)
            }
        default:
            return state2;
    }
}

const UseReducer = () => {
    const [state, dispatch] = useReducer(reducer, initialState)
    const [state2, numberDispatch] = useReducer(numberController, initialState)
    const [customNumber, setCustomNumber] = useState(0)

    function addCustom() {
        numberDispatch({type: 'add_custom', payload: {number: customNumber}})
        setCustomNumber(0)
    }

    return (
        <div className="UseReducer">
            <PageTitle
                title="Hook UseReducer"
                subtitle="Uma outra forma de ter estado em componentes funcionais!"
            />
            <SectionTitle title="UseReducer #1" />
            <div>
                <button 
                    className="btn" 
                    style={{maxWidth: '250px'}} 
                    onClick={() => dispatch({type: 'login_example'})}
                    >Login</button>
                <button 
                    className="btn" 
                    style={{maxWidth: '250px'}} 
                    onClick={() => login(dispatch, {name: 'Max Verstappen'})}
                    >Login 2</button>
                <button 
                    className="btn" 
                    style={{maxWidth: '250px'}} 
                    onClick={() => dispatch({type: 'login_example_3', payload: {name: 'Carlos Sainz', email: 'envkt@example.com'}})}
                    >Login 3</button>
            </div>
            <div className='center'>
                {state.user ? 
                <span className="text">{state.user.name}</span> : 
                <span className="text">Unauthenticated</span>
                }
                {state.user && state.user.email ? 
                <span>{state.user.email}</span> : ''
                }
            </div>
            <div className="center">
                <span className="text">{state.number}</span>
                <div>
                    <button className="btn" onClick={() => dispatch({type: 'number_add_1'})}>+ 1</button>
                    <button className="btn" onClick={() => numberAdd2(dispatch)}>+ 2</button>
                </div>
            </div>

            <SectionTitle title="UseReducer #2" />
            <div className="center">
                <span className="text">{state2.number}</span>
                <div>
                    <button className="btn" onClick={() => numberDispatch({type: 'multiply_by_7'})}>x 7</button>
                    <button className="btn" onClick={() => numberDispatch({type: 'divide_by_25'})}>/ 25</button>
                    <button className="btn" onClick={() => numberDispatch({type: 'add_custom', payload: {number: 100}})}>+ 100</button>
                    <button className="btn" onClick={() => numberDispatch({type: 'parse_number'})}>To Integer</button>
                    <button className="btn" onClick={() => addCustom()}>Add Custom</button>
                </div>
                <input type="number" className="input" value={customNumber} onChange={e => setCustomNumber(+e.target.value)} />
            </div>
        </div>
    )
}

export default UseReducer
