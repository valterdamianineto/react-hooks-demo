import { useEffect, useRef, useState } from 'react'
import PageTitle from '../../components/layout/pageTitle/PageTitle'
import SectionTitle from '../../components/layout/sectionTitle/SectionTitle'

function merge(val1: string, val2: string) {
    
    const list = val1.split('')
    return list.map((e, i) => {
        return `${e}${val2[i] || ''}`
    }).join("")
}

const UseRef = () => {
    const [value1, setValue1] = useState<string>("")
    const [value2, setValue2] = useState<string>("")
    const count = useRef(0)
    const myInput1 = useRef<any>(null)
    const myInput2 = useRef<any>(null)

    useEffect(function(): void {
        count.current = count.current + 1
        if(myInput2.current !== null) {
            myInput2.current.focus()
        }
    }, [value1, value2])


    useEffect(function() {
        count.current++
        if(myInput1.current !== null) {
            myInput1.current.focus()
        }
    }, [value2])

    return (
        <div className="UseRef">
            <PageTitle
                title="Hook UseRef"
                subtitle="Retorna um objeto mutável com a propriedade .current!"
            />
            <SectionTitle title="UseRef #1"/>
            <div className='center'>
                <div>
                    <span className="text">Valor: </span>
                    <span className="text">{merge(value1, value2)}</span>
                    <span className="text red">[{count.current}]</span>
                </div>
                <input ref={myInput1} type="text" className="input" value={value1} onChange={e => setValue1(e.target.value)} />
            </div>
            <SectionTitle title="UseRef #1"/>
            <div className="center">
                <input ref={myInput2} type="text" className="text" value={value2} onChange={e => setValue2(e.target.value)} />
            </div>
        </div>
    )
}

export default UseRef
