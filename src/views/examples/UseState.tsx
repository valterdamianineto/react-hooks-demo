import { useState } from 'react'
import PageTitle from '../../components/layout/pageTitle/PageTitle'
import SectionTitle from '../../components/layout/sectionTitle/SectionTitle'
const UseState = () => {
    const [current, setCurrent] = useState(0)
    const [name, setName] =useState('')

    return (
        <div className="UseState">
            <PageTitle
                title="Hook UseState"
                subtitle="Estado em componentes funcionais!"
            />
            <SectionTitle title="Use State #1" />
             <div className='center'>
                <span className="text">{current}</span>
                <div>
                    <button className="btn" onClick={() => setCurrent(current - 1)}>-1</button>
                    <button className="btn" onClick={() => setCurrent(0)}>Reset</button>
                    <button className="btn" onClick={() => setCurrent(current + 1)}>+1</button>
                </div>
             </div>
            <SectionTitle title="Use State #2" />
            <div className='center'>
                <input type="text" className="input" value={name} onChange={e => setName(e.target.value)}/>
            </div>

        </div>
    )
}

export default UseState
